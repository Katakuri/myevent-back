const mongoose = require("mongoose");
const colors = require("colors");


const url = "mongodb://localhost/myEvent";

const dbconfig = {
      useNewUrlParser: true,
      useFindAndModify : false
};

const connect = mongoose.connect(url, dbconfig);

connect.then((db) => {
    console.log(colors.bgGreen.white.bold("Connected to the DataBase "+ url));
}).catch((err) => {
    console.log(colors.bgRed.white.bold('Erreur lors de la connexion de la DataBase '+err));
});