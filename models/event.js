const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title : {
        type : String, 
        required : [true, 'title field is required']
    },

    category : {
        type : String,
        enum : ['Sport', 'Informatique', 'Cuisine', 'Informatique', 'LectureCulture', 'Photographie', 'Musique', 'BienEtre', 'Film', 'ModeBeaute', 'Art'],
        required : [true, 'category field is required']
    },

    description : {
        type : String,
        required : [true, 'description field is required'],
    },

    city_coord: {
        type : JSON,
        required : [true, 'city_coord field is required'],

        city : {
            type: String
        },

        coord : {
            lon : {
                type : Number
            },
            lat : {
                type : Number
            }
        },
    },

    nbAgeMin : {
        type : Number,
        required : [true, 'nbAgeMin field is required'],
    },

    dateStartEvent : {
        type : Date,
        default : Date.now()
        // required : [true, 'nbUserMax field is required'],
    },

    dateEndEvent : {
        type : Date,
        default : Date.now(),
        // required : [true, 'DateEnd field is required'],
    },

    nbParticipationMax : {
        type : Number,
        required : [true, 'nbUserMax field is required'],

    },

    idUsersParticipate : {
        type : []
    },

    createdBy : {
        type : String,
        default : 'Need Session Cookies or JWT'
    }
},
{collection : "events", timestamps : true});

const Event = mongoose.model('event', EventSchema);
module.exports = Event;