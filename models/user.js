const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    sexe : {
        type : String,
        required : [true, 'Sexe field is required'],
        enum: ['Homme', 'Femme']
    },

    firstName : {
        type : String,
        required : [true, 'Name field is required']
    },

    lastName : {
        type : String,
        required : [true, 'lastName field is required']
    },

    email : {
        type : String,
        index: { unique: true },
        required : [true, 'email field is required']
    },

    city : {
        type : String,
        required : [true, 'city field is required']
    },

    age : {
        type : Number,
        required : [true, 'age field is required'],
        max : 110
    },

    password : {
        type : String,
        required : [true, 'password field is required']
    }
},

{collection : "users", timestamps : true});

const User = mongoose.model('user', UserSchema);
module.exports = User;