# MyEvent -- Back



## Getting Started

Étapes à suivre pour lancer le projet en local

### Prerequisites

Installer MongoDB
Installer NodeJS
Installer Angular 


### Installing

Excecuter `npm install` pour installer toutes les dépendances présentes dans le projet.
Lancer MongoDB (port 27017).

Pour récupérer toutes les villes de France et utiliser la Map, il faut importer les villes depuis un fichier JSON (à faire une seule fois pour alimenter la DB)

Télécharger le fichier `city.list.json` et spécifier le bon chemin dans `routes/db.routes.js` s'il n'est pas au bon endroit.

Excecuter `node --max-old-space-size=4096 index.js` et executer la requete GET `http://localhost:4000/save/saveCitiesDB` qui va alimenter la DB.

Une fois les villes stockées dans la DB, le projet peut être lancé.

Executer `node index.js` pour lancer le serveur et ouvrir le `ProjetFront` Pour suivre le README propre au Front et utiliser l'application complète.

## Authors
* **Clarisse Vemba** 
* **Adame Benadjal** 
