require('./models/db');
require('./tools/schedule');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const userRoute = require('./routes/user.route');
const dbRoute = require('./routes/db.route');
const eventRoute = require('./routes/event.route');
const colors = require("colors");
const cors = require('cors');
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');

const app = express();


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('dev'));

// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api', userRoute);
app.use('/event', eventRoute);
app.use('/save', dbRoute);


app.use((err, req, res, next) => {
    res.status(422).send({ error : err.message});
    console.log(err);
});

app.listen(process.env.port || 4000, () => {
    console.log('Now listening for request');
});