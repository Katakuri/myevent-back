const express = require('express');
const router = express.Router();
const Event = require('../models/event');
const middleware = require('../middlewares/user.middlewares');
const jwt = require('jsonwebtoken');
const config = require('../tools/config');
const emailModule = require('../tools/email');
const request = require('request');


// Ajout d'un evenement
router.post('/addEvent',middleware.checkToken, (req,res,next) => {
    const usertoken = req.headers.authorization;
    const token = usertoken.split(' ');
    const decoded = jwt.verify(token[1], config.secret);
    username = decoded.username;
    req.body.createdBy = username
    Event.create(req.body).then((event) => { res.send(event)}).catch(next);
});

// Récup tous les evenements
router.get('/allEvents', middleware.checkToken, (req,res,next) => {
    Event.find({}).then((events) => {
        if(events){
            res.send(events);
        } else {
            res.send("Pas d'événements");
        }
    });
});

// Recup un evenement specifique
router.get('/eventById/:id', (req,res,next) => {
    Event.findOne({_id: req.params.id}).then((event) => 
    {  
        if(event){
            res.send(event);
        } else {
            res.send("ID incorrect");
        }
    }).catch(next);
});

// Modifie un evenement specifique
router.put('/updateEvent/:id', (req,res,next) => {
    Event.findByIdAndUpdate({_id : req.params.id}, req.body).then(() =>{ 
        Event.findById({_id : req.params.id}).then((event) => {
            res.send(event);
        })}).catch(next)
});

//Supression d'un evenement
router.delete('/deleteEvent/:id', (req,res,next) => {
    Event.findByIdAndDelete({_id : req.params.id}, req.body).then(() =>{ 
        res.send('Event deleted');
}).catch(next)
});

// Participer à un evenement 
router.post('/participatEvent/:id', middleware.checkToken, (req,res,next) => {
    id = req.params.id;
    var mailOpt = emailModule.mailOptions;
    var sendEmail = emailModule.sendEmail;

    // Get the username from the token
    const usertoken = req.headers.authorization;
    const token = usertoken.split(' ');
    const decoded = jwt.verify(token[1], config.secret);
    username = decoded.username;

    // Add the user in the list of particiapte
    Event.findByIdAndUpdate({_id : id}, { $push: {idUsersParticipate: username }}).then(() =>{ 
        Event.findById({_id : id}).then((event) => {
            if(event){
                mailOpt.subject = "Participation à l'evenement - " + event.title;
                mailOpt.to = 'myevent.miage@gmail.com,'+username;
                console.log(mailOpt.to);
                mailOpt.text = "Vous participez bien à l'evement : "+ event.title+" qui aura lieu le " + event.dateStartEvent;
                sendEmail(mailOpt);
                res.send(event);
            } else {
                res.send('ID incorrect');
            }
        })}).catch(next)
});

//Recup les evenements par catégories

router.get('/getEventsByCategory/:category',(req,res,next) => {
    cat = req.params.category;
    console.log(cat)

    Event.find({category : cat}).then((events) => {
        if(events){
            res.send(events)
        }else {
            res.send("Pas d'evenements de cette catégorie")
        }
    }).catch(next)
});

//Recup events par ville
router.get('/getEventsByCity/:city', (req,res,next) => {
        city = req.params.city;
        console.log(city);
    
        Event.find({city_coord : city}).then((events) => {
            if(events){
                res.send(events)
            } else {
                res.send("Pas d'evenements dans cette ville")
            }
        }).catch(next)
    });

// Recup events qui n'ont pas encore commencés
router.get('/getEventsDontStart', (req,res,next) => {
    Event.find({dateEndEvent : {$gte : new Date()}}).then( (events) => {
        if(events){
            res.send(events);
        } else {
            res.send("Aucun evenement")
        }
    }).catch(next)
});

// Recup les evenements terminés
router.get('/getEventCompleted', (req,res,next) => {
    Event.find({dateEndEvent : {$lt : new Date()}}).then( (events) => {
        if(events) {
            res.send(events);
        } else {
            res.send("Aucun evenement")
        }
    }).catch(next)
});

module.exports = router;