const express = require('express');
const router = express.Router();
const Citie = require('../models/citie');
const colors = require("colors");
const request = require('request');
const config = require('../tools/config');


// lancer avec "node --max-old-space-size=4096 index.js" car sinon plante pas assez d'espace
router.get('/saveCitiesDB', (req,res,next) => {
    const fs = require('fs');
    // Mettre le bon chemin du fichier "city.list.json"
    fs.readFile('/Users/adamebenadjal/Downloads/city.list.json', (err, data) => {
        if (err) throw err;
        let allCities = JSON.parse(data);

        // On prend les villes de France seulement
        allCities.forEach((citie) => {
            if(citie.country === "FR"){
                Citie.create(citie).then((citie) => {
                    console.log(colors.red('DONE'));
                });
            }
        });
        res.send('OK');
});
});

//Get all cities
router.get('/allCities', (req,res,next) => {
    Citie.find({}, (err, cities) => {
        if(err){
            res.statusCode(404);
        } else {
            res.send(cities);
        }
    });
});

// Get city by ID
router.get('/cityByID/:id', (req,res,next) => {
    id = req.params.id;
    console.log(id);

    Citie.find({id : id}, (err, city) => {
        if(err){
            res.statusCode(404);
        } else {
            res.send(city);
        }
    })
});

// Get city object by name

router.get('/cityByName/:name', (req,res,next) => {
    name = req.params.name;
    console.log(name);

    Citie.find({name : name}, (err,city) => {
        if(err){
            res.send('Pas de ville avec ce nom').statusCode(404);
        } else {
            res.send(city);
        }
    });
});

// Get the weather of a city 
router.get('/weatherCity/:name', (req,res,next) => {
    name = req.params.name;

    Citie.find({name : name}, (err, city) => {
        id = city[0].id;
        console.log(id);
        if(err){
            res.statusCode(404);
        } else {
            request('http://api.openweathermap.org/data/2.5/weather?id='+id+'&APPID='+config.APIKEYWEATHER, (error, response, body) => {
                if (!error && response.statusCode == 200) {
                    console.log('OK')
                    res.send(JSON.parse(body));
                }
        });
        }
    })
});

module.exports = router;