const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const emailModule = require('../tools/email');
const jwt = require('jsonwebtoken');
const config = require('../tools/config');
const middleware = require('../middlewares/user.middlewares');
const Event = require('../models/event');
const colors = require("colors");


// Enregistrer un nouveau compte
router.post('/register', (req, res, next) => {
    var mailOpt = emailModule.mailOptions;
    var sendEmail = emailModule.sendEmail;
    var lastName = req.body.lastName;
    var firstName = req.body.firstName;
    var email = req.body.email;

    // Préparation de l'email
    mailOpt.subject = 'Créeation de compte - '+ lastName +' '+ firstName;
    mailOpt.to = 'myevent.miage@gmail.com,'+email;
    console.log(mailOpt.to);
    mailOpt.text = "Votre compte a bien été enregistré dans l'application !";
    

    // Hasher le mdp puis enregistrer l'utilisateur et envoyer un mail de confirmation
    
    bcrypt.hash(req.body.password, 10 , (err, hash) => {
        if(err){
            res.statusCode(404);
            res.send(err);
        }else{
            req.body.password = hash;
            User.create(req.body).then((user) => {
                res.send(user);
                sendEmail(mailOpt);
            }).catch(next);
        }
    });
});

//Connexion d'un utilisateur
router.post('/login', (req,res, next) => {
        var login = req.body.email;
        var password = req.body.password;
        console.log(colors.bgRed.white.bold('request '+req.body));

        User.find({email : login}, (err, user) => {
            if(user.length === 0){
                res.send('Compte inexistant !');
            }
        }).then((user) => {
            // On récup le mot de passe de l'utilisateur renvoyé
            userPass = user[0].password;
            return bcrypt.compare(password, userPass);
        }).then((samePass) => {
            if(!samePass) {
                res.status(400).send('Mot de passe incorrect !');
            }else{
                let token = jwt.sign({username: login, motdepasse : password},
                    config.secret,
                    { expiresIn: '24h' // expires in 24 hours
                    }
                  );
                  // return the JWT token for the future API calls
                  res.json({
                    success: true,
                    message: 'Authentication successful!',
                    token: token
                  });
            }
        }).catch((err) => {
            console.log("Error authenticating user: ");
            console.log(err);
            res.status(400).send('Error authenticating user');

            next();
        });
});

//Recup tous les users
router.get('/getUsers',middleware.checkToken, (req,res,next) => {
    User.find({}, (err,users) => {
        if(err){
            res.statusCode(404);
        }else{
            res.send(users);
        }
    });
});

//Recup les evenements d'un user en particulier
router.get('/getEventsByUser', middleware.checkToken, (req,res,next) => {
    const usertoken = req.headers.authorization;
    const token = usertoken.split(' ');
    const decoded = jwt.verify(token[1], config.secret);

    username = decoded.username;

    Event.find({createdBy : username}, (err,events) => {
        if(err){
            res.send(err);
        }else {
            res.send(events);
        }
    });
});

router.delete('/deleteUser', middleware.checkToken, (req,res,next) => {
    const usertoken = req.headers.authorization;
    const token = usertoken.split(' ');
    const decoded = jwt.verify(token[1], config.secret);
    username = decoded.username;

    var mailOpt = emailModule.mailOptions;
    var sendEmail = emailModule.sendEmail;
    mailOpt.subject = 'Suppression du compte' ;
    mailOpt.to = 'myevent.miage@gmail.com,'+ username;
    console.log(mailOpt.to);
    mailOpt.text = "Votre compte a bien été supprimé de l'application !";

    User.findOneAndDelete({email : username}, (err, user) => {
        if(err){
            res.send(err);
        } else {
            sendEmail(mailOpt);
            res.send(user);
        }
    }).catch(next);
});

module.exports = router;