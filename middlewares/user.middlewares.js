let jwt = require('jsonwebtoken');
const config = require('../tools/config');
const mongoose = require('mongoose');
const User = require('../models/user');

let checkToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

  if (token) {
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
      }
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  }  else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
};

let getUserWithLogin = (req,res,next) => {
    login = req.body.email;

    let currentUser = User.find({email : login}).then( (res) => {
      return res.json({
        success : true,
        message : res
      });
    });
}

module.exports = {
  checkToken: checkToken,
  getUserWithLogin : getUserWithLogin
}